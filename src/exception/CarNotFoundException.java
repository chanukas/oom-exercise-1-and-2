package exception;

public class CarNotFoundException extends Exception{

    public CarNotFoundException(String exception){
        super(exception);
    }

}
