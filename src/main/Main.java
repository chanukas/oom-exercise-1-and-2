package main;

import exception.CarNotFoundException;

public class Main {

    // This is main method
    public static void main(String[] args) {

        try {
            new Main().validateCar("BMW");
            System.out.println("Car is found.");
        } catch (CarNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }

    // This is the validate car method
    void validateCar(String car) throws CarNotFoundException {

        // get all cars
        String[] allCars = getAllCars();

        boolean isCarAvailable = false;

        // loop car array and find if the car is available
        for (String carM : allCars) {

            if (carM.equals(car)) {
                isCarAvailable = true;
            }
        }

        // if it is not found throw car not found exception
        if (!isCarAvailable) throw new CarNotFoundException("Car is not found");

    }

    // This method returns all cars array
    static String[] getAllCars() {
        return new String[]{"Volvo", "BMW", "Ford", "Mazda"};
    }

}
